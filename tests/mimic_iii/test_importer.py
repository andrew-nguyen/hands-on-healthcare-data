#  Copyright (c) 2022-2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
#
import pandas as pd
import os

import database.graph as graph
from mimic_iii import importer

from helpers.databases import get_arango

basepath = os.path.join(os.path.dirname(__file__),
                        "../../")


def test_MIMIC_Importable():
    data = pd.read_csv(os.path.join(basepath,
                                    'resources/mimic-1.4-PRESCRIPTIONS-subset.csv')) \
        .fillna(value=0)

    a = get_arango('test_MIMIC_Importable', truncate=True)

    doc = importer.MIMIC_Importable(data.loc[0])

    assert doc.dict == {'row_id': 32600,
                        'subject_id': 42458,
                        'hadm_id': 159647,
                        'icustay_id': 0,
                        'startdate': '2146-07-21 00:00:00',
                        'enddate': '2146-07-22 00:00:00',
                        'drug_type': 'MAIN',
                        'drug': 'Pneumococcal Vac Polyvalent',
                        'drug_name_poe': 'Pneumococcal Vac Polyvalent',
                        'drug_name_generic': 'PNEUMOcoccal Vac Polyvalent',
                        'formulary_drug_cd': 'PNEU25I',
                        'gsn': 48548.0,
                        'ndc': 6494300.0,
                        'prod_strength': '25mcg/0.5mL Vial',
                        'dose_val_rx': 0.5,
                        'dose_unit_rx': 'mL',
                        'form_val_disp': 1,
                        'form_unit_disp': 'VIAL',
                        'route': 'IM'}

    assert doc.to_arango(graph.Default_Collections.PRESCRIPTIONS) == {
        'aql': 'INSERT @doc INTO prescriptions LET newDoc = NEW RETURN newDoc',
        'bind_vars': {
            'doc': {'_key': 32600,
                    'row_id': 32600,
                    'subject_id': 42458,
                    'hadm_id': 159647,
                    'icustay_id': 0,
                    'startdate': '2146-07-21 00:00:00',
                    'enddate': '2146-07-22 00:00:00',
                    'drug_type': 'MAIN',
                    'drug': 'Pneumococcal Vac Polyvalent',
                    'drug_name_poe': 'Pneumococcal Vac Polyvalent',
                    'drug_name_generic': 'PNEUMOcoccal Vac Polyvalent',
                    'formulary_drug_cd': 'PNEU25I',
                    'gsn': 48548.0,
                    'ndc': 6494300.0,
                    'prod_strength': '25mcg/0.5mL Vial',
                    'dose_val_rx': 0.5,
                    'dose_unit_rx': 'mL',
                    'form_val_disp': 1,
                    'form_unit_disp': 'VIAL',
                    'route': 'IM'}
        }
    }
