#  Copyright (c) 2022-2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
import database.graph as graph


def test_postgres():
    p = graph.PostgresDB()
    r = p.execute("select count(*) from prescriptions")
    # execute() uses fetchmany() so result is always a list
    # count(*) returns a tuple with one element so need to use two subscripts
    assert r[0][0] == 10398
