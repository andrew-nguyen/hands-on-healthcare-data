#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
import database.graph as graph
from database.graphdb import GraphDB


def test_graphdb_repository_functions():
    test_repo_name = "test-repo-2347524"

    # delete repo no matter what since we may have had a previously failed test
    GraphDB.delete_repository(test_repo_name)

    # first, confirm that repo doesn't exist
    g = GraphDB.repository_exists("somethingunkown")
    assert g == False

    # then we create repo
    g = GraphDB.create_repository(
        id=test_repo_name,
        title="Test Repo",
        baseurl="http://testrepo/graphdb#"
    )
    assert g == True

    # and confirm that the repo now exists
    g = GraphDB.repository_exists(test_repo_name)
    assert g == True

    # then delete
    g = GraphDB.delete_repository(test_repo_name)
    assert g == True

    # and confirm it no longer exists
    g = GraphDB.repository_exists(test_repo_name)
    assert g == False


def test_graphdb():
    g = GraphDB()
    result: dict
    result = g.execute({'query': """
        select * where {
            ?s ?p ?o .
        } limit 100
    """})
    # In a new repo, there are 70 triples
    assert len(result['results']['bindings']) >= 70

    result = g.insert(graph.Default_Collections.CONCEPTS, {
        'rxcui': "somerxcui",
        'str': "some label"
    })

    assert result == b''
