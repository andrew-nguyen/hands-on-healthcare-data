#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
import pytest

import database.graph as graph
from helpers.databases import get_arango

def test_ArangoDB():
    with pytest.raises(ValueError):
        graph.ArangoDB()

    with pytest.raises(ValueError):
        graph.ArangoDB(
            password="",
            url="http://127.0.0.1:8529",
            dbname="test")

    with pytest.raises(ValueError):
        graph.ArangoDB(username="",
                       url="http://127.0.0.1:8529",
                       dbname="test")

    with pytest.raises(ValueError):
        graph.ArangoDB(username="",
                       password="",
                       dbname="test")

    with pytest.raises(ValueError):
        graph.ArangoDB(username="",
                       password="",
                       url="http://127.0.0.1:8529")

    a = get_arango("test_ArangoDB", truncate=True)

    assert len(a.collections) == len(graph.Default_Collections)

    for c in graph.Default_Collections:
        assert a.db.has_collection(c.value)

    assert a.sys_db.has_database(a.dbname)

    a.drop_database()

    assert not a.sys_db.has_database(a.dbname)


def test_arango_graph():
    a = get_arango("test_arango_graph", truncate=True)

    a.insert(graph.Default_Collections.CONCEPTS, {'rxcui': "1234",
                                          'ndc': "0001234",
                                          'str': "Some Concept"})

    a.insert(graph.Default_Collections.CONCEPTS, {'rxcui': "1235",
                                          'ndc': "0001235",
                                          'str': "Some Concept"})

    a.insert(graph.Default_Collections.CONCEPTS, {'rxcui': "1236",
                                          'ndc': "0001236",
                                          'str': "Some Concept"})

    a.insert(graph.Default_Collections.PRESCRIPTIONS, {'ndc': "0001234",
                                               'str': "Some prescription 1234"})

    a.insert(graph.Default_Collections.PRESCRIPTIONS, {'ndc': "0001234",
                                               'str': "Some prescription "
                                                      "1234.2"})

    a.insert(graph.Default_Collections.PRESCRIPTIONS, {'ndc': "0001235",
                                               'str': "Some prescription 1235"})

    a.insert(graph.Default_Collections.PRESCRIPTIONS, {'ndc': "0001237",
                                               'str': "Some prescription 1237"})

    assert a.collection(graph.Default_Collections.CONCEPTS).count() == 3
    assert a.collection(graph.Default_Collections.PRESCRIPTIONS).count() == 4

    # test using raw queries

    a.execute({
        'aql': """
        FOR p in prescriptions
            FOR c in concepts
                FILTER p.ndc == c.ndc

                INSERT {
                    _from: p._id,
                    _to: c._id

                } in instance_of
                RETURN NEW
        """,
        'bind_vars': {}
    })

    assert a.edges[graph.Default_Edges.INSTANCE_OF].count() == 3

    result = [doc for doc in a.execute({
        'aql': """
        FOR c in concepts
            FILTER c.ndc == '0001234'
            FOR v,e,p
                IN 
                ANY c
                GRAPH 'chapter4'
                RETURN {v: v}
        """,
        'bind_vars': {}
    })]

    assert len(result) == 2

    result = [doc for doc in a.execute({
        'aql': """
            FOR c in concepts
                FILTER c.ndc == '0001235'
                FOR v,e,p
                    IN 
                    ANY c
                    GRAPH 'chapter4'
                    RETURN {v: v}
            """,
        'bind_vars': {}
    })]

    assert len(result) == 1

    result = [doc for doc in a.execute({
        'aql': """
            FOR c in concepts
                FILTER c.ndc == '0001236'
                FOR v,e,p
                    IN 
                    ANY c
                    GRAPH 'chapter4'
                    RETURN {v: v}
            """,
        'bind_vars': {}
    })]

    assert len(result) == 0

    a.edges[graph.Default_Edges.INSTANCE_OF].truncate()

    assert a.edges[graph.Default_Edges.INSTANCE_OF].count() == 0

    # test using py-arango API

    e = a.edges[graph.Default_Edges.INSTANCE_OF]
    concepts = a.collection(graph.Default_Collections.CONCEPTS).all()

    for c in concepts:
        for p in a.collection(graph.Default_Collections.PRESCRIPTIONS).find({
            'ndc': c['ndc']
        }):
            e.link(p, c)

    assert a.edges[graph.Default_Edges.INSTANCE_OF].count() == 3

    p = e.edges(a.collection(graph.Default_Collections.CONCEPTS).find({
        'ndc': '0001234'
    }).next(), direction='in')['edges']
    assert len(p) == 2

    p = e.edges(a.collection(graph.Default_Collections.CONCEPTS).find({
        'ndc': '0001235'
    }).next(), direction='in')['edges']
    assert len(p) == 1

    p = e.edges(a.collection(graph.Default_Collections.CONCEPTS).find({
        'ndc': '0001236'
    }).next(), direction='in')['edges']
    assert len(p) == 0

    # a.drop_database()

# def test_ArangoTyping():
#     a = get_arango()
#     a.execute({"aql": "something", "bind_vars": {'something': 12}})
