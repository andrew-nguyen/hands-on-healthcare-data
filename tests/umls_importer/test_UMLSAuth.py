#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
#
#
import pytest, os

from umls_importer.UMLSAuth import UMLSAuth

def test_UMLSAuth():
    a = UMLSAuth(os.environ.get("UMLS_APIKEY"))
    st = a.getst()
