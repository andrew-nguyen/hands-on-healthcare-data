#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
import pytest
import os

import umls_importer.umls as umls
import umls_importer.UMLSAuth as UMLSAuth
import database.graph as graph

from helpers.databases import get_arango


def test_UMLSAPI():
    auth = UMLSAuth.UMLSAuth(os.environ.get("UMLS_APIKEY"))
    api = umls.UMLSAPI(auth)

    resp = api.get_content_view("")
    assert resp[0] == 404

    resp = api.get_content_view("C3503753")  # RxNorm prescribable content view
    assert resp[0] == 200
    assert resp[1]['result']['classType'] == "ContentView"
    # print(json.dumps(resp[1], indent=4, sort_keys=True))

    resp = api.get_content_view_members(
        "C3503753")  # RxNorm prescribable content
    # view
    assert resp[0] == 200
    assert resp[1]['pageSize'] == 5
    # print(json.dumps(resp[1], indent=4, sort_keys=True))


def test_RxNorm():
    auth = UMLSAuth.UMLSAuth(os.environ.get("UMLS_APIKEY"))
    api = umls.UMLSAPI(auth)

    resp = api.get_NDC_properties('63323026201')
    assert resp[0] == 200
    assert resp[1]['ndcPropertyList']['ndcProperty'][0][
               'ndcItem'] == "63323026201"
    assert resp[1]['ndcPropertyList']['ndcProperty'][0]['rxcui'] == "1361615"
    assert umls.rxcui_from_ndc_properties(resp) == "1361615"

    resp = api.get_NDC_status('63323026201')
    assert resp[0] == 200
    assert umls.rxcui_from_ndc_status(resp) == "1361615"

    # obsolete code
    resp = api.get_NDC_status('00904516561')
    assert resp[0] == 200
    assert umls.rxcui_from_ndc_status(resp) == "312935"
    # print(json.dumps(resp[1], indent=4, sort_keys=True))


def test_UMLSConcept():
    c = umls.UMLSConcept(
        rxcui="11111"
    )
    assert c.to_arango(graph.Default_Collections.CONCEPTS) == {
        'aql': 'INSERT @doc INTO concepts LET newDoc = NEW RETURN newDoc',
        'bind_vars': {'doc': {'cui': None,
                              'str': None,
                              'rxcui': "11111",
                              'ndc': None}}}

    assert c.to_sparql(graph.Default_Collections.CONCEPTS, id=1234) == {
        'query': """
INSERT DATA {
    hohcd:1234 rdf:type hohcd:concepts .
    hohcd:1234 rxnorm:RXCUI "11111" .
}        
""".strip()
    }

    with pytest.raises(NotImplementedError):
        c.to_neo4j()

    c.cui = "C1234"
    c.str = "FakeConcept"

    assert c.as_dict() == {'cui': 'C1234',
                           'str': 'FakeConcept',
                           'rxcui': '11111',
                           'ndc': None}


def test_UMLSConcept_arango_execute():
    a = get_arango('test_UMLSConcept_arango_execute', truncate=True)

    c = umls.UMLSConcept()
    c.cui = "C1234"
    c.str = "FakeConcept"

    assert a.collection(graph.Default_Collections.CONCEPTS).count() == 0

    a.execute(c.to_arango(graph.Default_Collections.CONCEPTS))

    assert a.collection(graph.Default_Collections.CONCEPTS).count() == 1

    a.drop_database()

def test_UMLSConcept_arango_insert():
    a = get_arango('test_UMLSConcept_arango_insert', truncate=True)

    c = umls.UMLSConcept()
    c.cui = "C1234"
    c.str = "FakeConcept"

    assert a.collection(graph.Default_Collections.CONCEPTS).count() == 0

    a.insert(graph.Default_Collections.CONCEPTS, c.as_dict())

    assert a.collection(graph.Default_Collections.CONCEPTS).count() == 1

    a.drop_database()