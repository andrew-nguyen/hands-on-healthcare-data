#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
#
#
import umls_importer.api as api


def test_inc():
    assert api.inc(4) == 5

