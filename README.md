# Hands-On Healthcare Data

This repository contains source code, examples, and other related materials to
accompany the book _Hands-On Healthcare Data_.

As with any open source project, the code contained within this repository is
intended to benefit the greater health informatics community and I appreciate
any comments, feedback, and pull requests.

## Working with UMLS

This project provides some source code and examples for working with the UMLS
within the context of several different types of databases.  The code is
written such that it should be pretty easy to adapt to any other database.

The UMLS is a fairly complex project that captures several levels of
granularity when navigating biomedical concepts and their written
representations.  This project is focused mainly on capturing the concepts
(e.g., concept unique identifiers or CUI's) and some labels (e.g., string
unique identifiers or SUI's) as well as semantic relationships where available.

If you would like to extend this library to support additional features of the
UMLS, please feel free to submit a pull request!

Copyright 2022 © Andrew Nguyen
