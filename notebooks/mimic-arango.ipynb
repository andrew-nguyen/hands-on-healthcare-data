{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# MIMIC and ArangoDB\n",
    "\n",
    "This notebook contains code that imports the MIMIC data into ArangoDB, a\n",
    "multi-model database that acts as both a document-oriented and property\n",
    "graph database.\n",
    "\n",
    "It uses pandas/numpy to to read and process data as dataframes.  It also uses\n",
    "several functions created specifically to accompany _Hands-On Healthcare Data_.\n",
    "\n",
    "The environment variable `UMLS_APIKEY` must be set -- it can be found in your\n",
    " [UTS profile](https://uts.nlm.nih.gov/uts/edit-profile)\n",
    "\n",
    "This notebook also assumes that you have ArangoDB running as a local docker\n",
    "container using a command similar to the following:\n",
    "\n",
    "```shell\n",
    "docker run --platform linux/amd64 `# running an M1 mac` \\\n",
    "           --name umls-arangodb `# naming it since starting detached` \\\n",
    "           -d `# detach` \\\n",
    "           -e ARANGO_NO_AUTH=1 `# disable auth for ArangoDB` \\\n",
    "           -p 8529:8529 `# Expose the port on host IP` \\\n",
    "           -v $HOME/data/arangodb:/var/lib/arangodb3 `# shared dir for arango` \\\n",
    "           -v $HOME/data/sources:/var/sources `# shared dir with data sources` \\\n",
    "           arangodb:3.8 `# use 3.8.x branch`\n",
    "```"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "import pandas as pd\n",
    "import pprint\n",
    "\n",
    "import umls_importer.umls as umls\n",
    "import database.graph as graph\n",
    "import mimic_iii.importer as importer\n",
    "\n",
    "# Sets up autoreload of modules every time a cell is executed\n",
    "%load_ext autoreload\n",
    "%autoreload 2"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Creating an ArangoDB connection\n",
    "\n",
    "Next, we use a helper function to establish a connection to the ArangoDB\n",
    "instance running in our Docker environment.  The details below assume that\n",
    "the standard port (8529) is being used and exposed via _localhost_.  Please\n",
    "edit these details if you are running ArangoDB on any other IP address or port.\n",
    "\n",
    "`username` and `password` are ignored if you started the ArangoDB instance per\n",
    "chapter 2 since we configured it to ignore authentication.\n",
    "\n",
    "`dbname` is set to \"examples\" and can be changed if you want.\n",
    "\n",
    "`truncate` tells the driver to ensure that the specified `dbname` is dropped\n",
    "and created such that any data stored within it is deleted.  This ensures\n",
    "that the code in this notebook starts with the same data every single time.\n",
    "\n",
    "If you wish to make changes to the underlying database and don't want to lose\n",
    " your work, please set `truncate` to False."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "outputs": [],
   "source": [
    "a = graph.ArangoDB(username=\"\",\n",
    "                   password=\"\",\n",
    "                   url=\"http://127.0.0.1:8529\",\n",
    "                   dbname=\"examples\",\n",
    "                   truncate=True)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Load Medication Concepts\n",
    "\n",
    "I have already preprocessed medication concepts for you and stored them in a\n",
    "CSV file.  Since there are quite a few medication concepts, I extracted only\n",
    "those medications that appear in the MIMIC-III demo database.  This saves us\n",
    "the time and space of downloading medication concepts that we will not use\n",
    "with this particular dataset."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "RXCUI Concepts shape: (997, 7)\n",
      "  Unnamed: 0                         drug                drug_name_poe  \\\n",
      "0          0  Pneumococcal Vac Polyvalent  Pneumococcal Vac Polyvalent   \n",
      "1          1                    Bisacodyl                    Bisacodyl   \n",
      "2          2                    Bisacodyl                    Bisacodyl   \n",
      "3          3                        Senna                        Senna   \n",
      "4          4     Docusate Sodium (Liquid)     Docusate Sodium (Liquid)   \n",
      "\n",
      "             drug_name_generic          ndc    rxcui  \\\n",
      "0  PNEUMOcoccal Vac Polyvalent  00006494300  1658472   \n",
      "1                    Bisacodyl  00536338101   308753   \n",
      "2           Bisacodyl (Rectal)  00574705050   198509   \n",
      "3                        Senna  00904516561   312935   \n",
      "4     Docusate Sodium (Liquid)  00121054410  1248119   \n",
      "\n",
      "                                         conceptName  \n",
      "0  0.5 ML Streptococcus pneumoniae type 1 capsula...  \n",
      "1         bisacodyl 5 MG Delayed Release Oral Tablet  \n",
      "2                 bisacodyl 10 MG Rectal Suppository  \n",
      "3                 sennosides, USP 8.6 MG Oral Tablet  \n",
      "4           docusate sodium 10 MG/ML Oral Suspension  \n",
      "Imported 997 rows\n"
     ]
    }
   ],
   "source": [
    "data = umls.read_rxcui_csv('mimic_1.4_demo_drugs_rxcui.csv')\n",
    "print(f\"RXCUI Concepts shape: {data.shape}\")\n",
    "print(data.head())\n",
    "\n",
    "c: umls.UMLSConcept\n",
    "counter = 0\n",
    "for c in data.apply(lambda x: umls.UMLSConcept(str=x.drug,\n",
    "                                               rxcui=x.rxcui,\n",
    "                                               ndc=x.ndc),\n",
    "                    axis='columns'):\n",
    "    # print(f\"Saving '{c}'\")\n",
    "    a.execute(c.to_arango(graph.Collections.CONCEPTS))\n",
    "    counter = counter + 1\n",
    "\n",
    "print(f\"Imported {counter} rows\")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Load MIMIC-III Prescriptions\n",
    "\n",
    "Here, we load the prescription data from the MIMIC-III demo data directly\n",
    "from the source CSV file.  No processing or manipulation is done to the data\n",
    "except to read everything as a string."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PRESCRIPTIONS shape: (10398, 19)\n",
      "  row_id subject_id hadm_id icustay_id            startdate  \\\n",
      "0  32600      42458  159647         NA  2146-07-21 00:00:00   \n",
      "1  32601      42458  159647         NA  2146-07-21 00:00:00   \n",
      "2  32602      42458  159647         NA  2146-07-21 00:00:00   \n",
      "3  32603      42458  159647         NA  2146-07-21 00:00:00   \n",
      "4  32604      42458  159647         NA  2146-07-21 00:00:00   \n",
      "\n",
      "               enddate drug_type                         drug  \\\n",
      "0  2146-07-22 00:00:00      MAIN  Pneumococcal Vac Polyvalent   \n",
      "1  2146-07-22 00:00:00      MAIN                    Bisacodyl   \n",
      "2  2146-07-22 00:00:00      MAIN                    Bisacodyl   \n",
      "3  2146-07-22 00:00:00      MAIN                        Senna   \n",
      "4  2146-07-21 00:00:00      MAIN     Docusate Sodium (Liquid)   \n",
      "\n",
      "                 drug_name_poe            drug_name_generic formulary_drug_cd  \\\n",
      "0  Pneumococcal Vac Polyvalent  PNEUMOcoccal Vac Polyvalent           PNEU25I   \n",
      "1                    Bisacodyl                    Bisacodyl             BISA5   \n",
      "2                    Bisacodyl           Bisacodyl (Rectal)           BISA10R   \n",
      "3                        Senna                        Senna           SENN187   \n",
      "4     Docusate Sodium (Liquid)     Docusate Sodium (Liquid)          DOCU100L   \n",
      "\n",
      "      gsn          ndc     prod_strength dose_val_rx dose_unit_rx  \\\n",
      "0  048548  00006494300  25mcg/0.5mL Vial         0.5           mL   \n",
      "1  002947  00536338101          5 mg Tab          10           mg   \n",
      "2  002944  00574705050  10mg Suppository          10           mg   \n",
      "3  019964  00904516561          1 Tablet           1          TAB   \n",
      "4  003017  00121054410      100mg UD Cup         100           mg   \n",
      "\n",
      "  form_val_disp form_unit_disp route  \n",
      "0             1           VIAL    IM  \n",
      "1             2            TAB    PO  \n",
      "2             1           SUPP    PR  \n",
      "3             1            TAB    PO  \n",
      "4             1          UDCUP    PO  \n",
      "Imported 10398 rows\n"
     ]
    }
   ],
   "source": [
    "prescriptions = pd.read_csv('mimic-1.4-demo/PRESCRIPTIONS.csv',\n",
    "                            dtype=str).fillna(value=\"NA\")\n",
    "print(f\"PRESCRIPTIONS shape: {prescriptions.shape}\")\n",
    "print(prescriptions.head())\n",
    "\n",
    "row: importer.MIMIC_Importable\n",
    "counter = 0\n",
    "for row in prescriptions.apply(lambda x: importer.MIMIC_Importable(x),\n",
    "                               axis='columns'):\n",
    "    a.execute(row.to_arango(graph.Collections.PRESCRIPTIONS))\n",
    "    counter = counter + 1\n",
    "\n",
    "print(f\"Imported {counter} rows\")"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Connecting Prescriptions and Concepts\n",
    "\n",
    "So, we now have the prescriptions loaded into the database as well as\n",
    "medication concepts.  The first thing we will want to do is connect each\n",
    "prescription entry (i.e., each row in the `PRESCRIPTIONS` table) to the\n",
    "corresponding medication concept.\n",
    "\n",
    "The CSV file that we used to load the medication concepts contains both the\n",
    "RXCUI (RxNorm concept unique identifier) as well as the corresponding NDC\n",
    "code.  This makes it much easier for us to connect the entries in the\n",
    "MIMIC-III data to the concepts.  Had we not done this, we would need to\n",
    "perform an additional lookup or we would need to track the mapping of NDC\n",
    "codes some other way."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Using Arango Query Language (AQL)\n",
    "\n",
    "Here, we use AQL to connect the prescriptions and medication concepts.  In\n",
    "the next section, we will use the python-arango python driver API."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "outputs": [
    {
     "data": {
      "text/plain": "{'filtered': 0,\n 'peakMemoryUsage': 1212416,\n 'modified': 10397,\n 'ignored': 0,\n 'scanned_full': 997,\n 'scanned_index': 10397,\n 'execution_time': 1.5536373340000864,\n 'http_requests': 0}"
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cursor = a.execute({\n",
    "    'aql': \"\"\"\n",
    "    FOR p in prescriptions\n",
    "        FOR c in concepts\n",
    "            FILTER p.ndc == c.ndc\n",
    "\n",
    "            INSERT {\n",
    "                _from: p._id,\n",
    "                _to: c._id\n",
    "\n",
    "            } in instance_of\n",
    "            RETURN NEW\n",
    "    \"\"\",\n",
    "    'bind_vars': {}\n",
    "})\n",
    "\n",
    "cursor.statistics()"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Using python-arango\n",
    "\n",
    "This section is commented out to prevent duplicate creation of edges (since\n",
    "we are also running the AQL version above) but shows how we could run the\n",
    "same query using the python-arango API.\n",
    "\n",
    "The query above is run using the database engine and is much faster.  The\n",
    "query below requires a \"roundtrip\" of data from the database to python and\n",
    "back, thus requiring significantly more time to execute."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "outputs": [],
   "source": [
    "# for c in a.collection(graph.Collections.CONCEPTS).all():\n",
    "#     for p in a.collection(graph.Collections.PRESCRIPTIONS).find({\n",
    "#         'ndc': c['ndc']\n",
    "#     }):\n",
    "#         a.edge(graph.Edges.INSTANCE_OF).link(p, c)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Mapping a New Heparin Concept\n",
    "\n",
    "As we discussed in chapter 3, there are a few different medication concepts\n",
    "that contain 'heparin' in the name.  Some of these are used to prevent clots\n",
    "in tubing/catheters and not for therapeutic purposes.  So, our goal here is\n",
    "to great a concept that represents heparin that is used therapeutically."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'_id': 'concepts/1312428',\n",
      " '_key': '1312428',\n",
      " '_rev': '_de0BiLi---',\n",
      " 'created': '2021-12-30T22:43:53.170357',\n",
      " 'purl': 'http://purl.foo.com/therapeutic-heparin',\n",
      " 'str': 'Heparin (therapeutic)',\n",
      " 'type': 'custom'}\n"
     ]
    }
   ],
   "source": [
    "hep_purl = 'http://purl.foo.com/therapeutic-heparin'\n",
    "hep_concept = a.insert(graph.Collections.CONCEPTS, {\n",
    "    'str': \"Heparin (therapeutic)\",\n",
    "    'type': 'custom',\n",
    "    'created': datetime.utcnow().isoformat(),\n",
    "    'purl': hep_purl\n",
    "}).next()\n",
    "\n",
    "pprint.pprint(hep_concept)"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "As you can see, I added two attributes, `type` and `created`, that allow us\n",
    "to separate out these concepts from the other concepts loaded in the concept\n",
    "table.  The intent here is to be able to query for concepts that come as part\n",
    " of standardized sources (such as RxNorm) vs. concepts that we define on our\n",
    " own.  There other ways to accomplish this such as storing the custom\n",
    " concepts in a new collection.  Ultimately, this decision will depend on many\n",
    "  other factors such as how you plan on managing and maintaining the custom\n",
    "  concepts and how they may be used in other queries.\n",
    "\n",
    "For this section, we will just track them using the `type` attribute.\n",
    "\n",
    "Next, we need to identify all of the instances of heparin:"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "outputs": [
    {
     "data": {
      "text/plain": "       _key               _id         _rev  cui  \\\n0   1278207  concepts/1278207  _de0AcLC---  NaN   \n1   1278270  concepts/1278270  _de0AcWe---  NaN   \n2   1278532  concepts/1278532  _de0Aepy---  NaN   \n3   1278652  concepts/1278652  _de0Ae4G---  NaN   \n4   1278718  concepts/1278718  _de0AfAW---  NaN   \n5   1279327  concepts/1279327  _de0AgKW---  NaN   \n6   1280512  concepts/1280512  _de0Aic2---  NaN   \n7   1280728  concepts/1280728  _de0Ai3C---  NaN   \n8   1280734  concepts/1280734  _de0Ai3m---  NaN   \n9   1280788  concepts/1280788  _de0Aj-O---  NaN   \n10  1280881  concepts/1280881  _de0AjJG---  NaN   \n11  1312428  concepts/1312428  _de0BiLi---  NaN   \n\n                                  str    rxcui          ndc    type  \\\n0                             Heparin  1361615  63323026201     NaN   \n1         Heparin Flush (10 units/ml)  1362055  08290036005     NaN   \n2                      Heparin Sodium  1658717  00338055002     NaN   \n3                             Heparin  1361615  00641040025     NaN   \n4   Heparin Flush CVL  (100 units/ml)  1362063  64253033335     NaN   \n5                      Heparin Sodium   848335  00074779362     NaN   \n6                      Heparin Flush    487906  17191003500     NaN   \n7                      Heparin Sodium  1658717  00264958720     NaN   \n8                      Heparin Sodium  1658717  00409779362     NaN   \n9                      Heparin Sodium  1362831  63323054207     NaN   \n10                 Heparin Lock Flush  1361029  00409115170     NaN   \n11              Heparin (therapeutic)      NaN          NaN  custom   \n\n                       created                                     purl  \n0                          NaN                                      NaN  \n1                          NaN                                      NaN  \n2                          NaN                                      NaN  \n3                          NaN                                      NaN  \n4                          NaN                                      NaN  \n5                          NaN                                      NaN  \n6                          NaN                                      NaN  \n7                          NaN                                      NaN  \n8                          NaN                                      NaN  \n9                          NaN                                      NaN  \n10                         NaN                                      NaN  \n11  2021-12-30T22:43:53.170357  http://purl.foo.com/therapeutic-heparin  ",
      "text/html": "<div>\n<style scoped>\n    .dataframe tbody tr th:only-of-type {\n        vertical-align: middle;\n    }\n\n    .dataframe tbody tr th {\n        vertical-align: top;\n    }\n\n    .dataframe thead th {\n        text-align: right;\n    }\n</style>\n<table border=\"1\" class=\"dataframe\">\n  <thead>\n    <tr style=\"text-align: right;\">\n      <th></th>\n      <th>_key</th>\n      <th>_id</th>\n      <th>_rev</th>\n      <th>cui</th>\n      <th>str</th>\n      <th>rxcui</th>\n      <th>ndc</th>\n      <th>type</th>\n      <th>created</th>\n      <th>purl</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr>\n      <th>0</th>\n      <td>1278207</td>\n      <td>concepts/1278207</td>\n      <td>_de0AcLC---</td>\n      <td>NaN</td>\n      <td>Heparin</td>\n      <td>1361615</td>\n      <td>63323026201</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>1</th>\n      <td>1278270</td>\n      <td>concepts/1278270</td>\n      <td>_de0AcWe---</td>\n      <td>NaN</td>\n      <td>Heparin Flush (10 units/ml)</td>\n      <td>1362055</td>\n      <td>08290036005</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>2</th>\n      <td>1278532</td>\n      <td>concepts/1278532</td>\n      <td>_de0Aepy---</td>\n      <td>NaN</td>\n      <td>Heparin Sodium</td>\n      <td>1658717</td>\n      <td>00338055002</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>3</th>\n      <td>1278652</td>\n      <td>concepts/1278652</td>\n      <td>_de0Ae4G---</td>\n      <td>NaN</td>\n      <td>Heparin</td>\n      <td>1361615</td>\n      <td>00641040025</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>4</th>\n      <td>1278718</td>\n      <td>concepts/1278718</td>\n      <td>_de0AfAW---</td>\n      <td>NaN</td>\n      <td>Heparin Flush CVL  (100 units/ml)</td>\n      <td>1362063</td>\n      <td>64253033335</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>5</th>\n      <td>1279327</td>\n      <td>concepts/1279327</td>\n      <td>_de0AgKW---</td>\n      <td>NaN</td>\n      <td>Heparin Sodium</td>\n      <td>848335</td>\n      <td>00074779362</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>6</th>\n      <td>1280512</td>\n      <td>concepts/1280512</td>\n      <td>_de0Aic2---</td>\n      <td>NaN</td>\n      <td>Heparin Flush</td>\n      <td>487906</td>\n      <td>17191003500</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>7</th>\n      <td>1280728</td>\n      <td>concepts/1280728</td>\n      <td>_de0Ai3C---</td>\n      <td>NaN</td>\n      <td>Heparin Sodium</td>\n      <td>1658717</td>\n      <td>00264958720</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>8</th>\n      <td>1280734</td>\n      <td>concepts/1280734</td>\n      <td>_de0Ai3m---</td>\n      <td>NaN</td>\n      <td>Heparin Sodium</td>\n      <td>1658717</td>\n      <td>00409779362</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>9</th>\n      <td>1280788</td>\n      <td>concepts/1280788</td>\n      <td>_de0Aj-O---</td>\n      <td>NaN</td>\n      <td>Heparin Sodium</td>\n      <td>1362831</td>\n      <td>63323054207</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>10</th>\n      <td>1280881</td>\n      <td>concepts/1280881</td>\n      <td>_de0AjJG---</td>\n      <td>NaN</td>\n      <td>Heparin Lock Flush</td>\n      <td>1361029</td>\n      <td>00409115170</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>NaN</td>\n    </tr>\n    <tr>\n      <th>11</th>\n      <td>1312428</td>\n      <td>concepts/1312428</td>\n      <td>_de0BiLi---</td>\n      <td>NaN</td>\n      <td>Heparin (therapeutic)</td>\n      <td>NaN</td>\n      <td>NaN</td>\n      <td>custom</td>\n      <td>2021-12-30T22:43:53.170357</td>\n      <td>http://purl.foo.com/therapeutic-heparin</td>\n    </tr>\n  </tbody>\n</table>\n</div>"
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results = [doc for doc in a.execute({\n",
    "    'aql': \"\"\"\n",
    "    FOR c in concepts\n",
    "        FILTER LOWER(c.str) LIKE '%heparin%'\n",
    "        RETURN c\n",
    "    \"\"\",\n",
    "    'bind_vars': {}\n",
    "})]\n",
    "\n",
    "results = pd.DataFrame.from_records(results)\n",
    "\n",
    "results"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "Based on the string name of these, we will link our new concept to those\n",
    "items with the following rxcui's:\n",
    "\n",
    "848335\n",
    "1361615\n",
    "1362831\n",
    "1658717"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'execution_time': 0.011321999991196208,\n",
      " 'filtered': 0,\n",
      " 'http_requests': 0,\n",
      " 'ignored': 0,\n",
      " 'modified': 7,\n",
      " 'peakMemoryUsage': 32768,\n",
      " 'scanned_full': 0,\n",
      " 'scanned_index': 7}\n"
     ]
    }
   ],
   "source": [
    "rxcuis_to_map = [\n",
    "    \"848335\",\n",
    "    \"1361615\",\n",
    "    \"1362831\",\n",
    "    \"1658717\"\n",
    "]\n",
    "\n",
    "cursor = a.execute({\n",
    "    'aql': \"\"\"\n",
    "    FOR c in concepts\n",
    "        FILTER c.rxcui IN @concepts\n",
    "\n",
    "        INSERT {\n",
    "            _from: c._id,\n",
    "            _to: @hep_concept._id\n",
    "        } in maps_to\n",
    "    \"\"\",\n",
    "    'bind_vars': {\n",
    "        'concepts': rxcuis_to_map,\n",
    "        'hep_concept': hep_concept\n",
    "    }\n",
    "})\n",
    "\n",
    "pprint.pprint(cursor.statistics())"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "### Querying Related Prescriptions\n",
    "\n",
    "Now that we have mapped various concepts to our new concept for heparin that\n",
    "is used therapeutically, we can query for prescriptions related to it without\n",
    " needing to worry about the intermediate concepts.\n",
    "\n",
    "First, we filter on the PURL that corresponds to our new concept:\n",
    "\n",
    "```\n",
    "FOR c in concepts\n",
    "        FILTER c.purl == 'http://purl.foo.com/therapeutic-heparin'\n",
    "```\n",
    "\n",
    "Then, we use it as our starting vertex for a graph query:\n",
    "\n",
    "```\n",
    "FOR meds IN INBOUND c maps_to\n",
    "    FOR prescription IN INBOUND meds instance_of\n",
    "```\n",
    "\n",
    "Remember, `c` is the concept returned when we filtered on the specified PURL\n",
    "and is used as the starting vertex.  From there, we find any concepts that\n",
    "point to it via the `maps_to` edge (contained in the variable `meds`).  Then,\n",
    " we follow the `instance_of` edge to find any prescriptions linked to `meds`."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 223 prescription entries\n"
     ]
    }
   ],
   "source": [
    "results = [doc for doc in a.execute({\n",
    "    'aql': \"\"\"\n",
    "    FOR c in concepts\n",
    "        FILTER c.purl == @purl\n",
    "        FOR meds IN INBOUND c maps_to\n",
    "            FOR prescription IN INBOUND meds instance_of\n",
    "                RETURN prescription\n",
    "    \"\"\",\n",
    "    'bind_vars': {\n",
    "        'purl': hep_purl\n",
    "    }\n",
    "})]\n",
    "\n",
    "# expect 223 entries\n",
    "print(f'Found {len(results)} prescription entries')"
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  },
  {
   "cell_type": "markdown",
   "source": [
    "While the syntax of AQL is a bit clunky (especially when compared to Cypher),\n",
    "we can see that the query itself does not need to be concerned with the\n",
    "clinical meaning (i.e., semantics) of the different types of heparin that may\n",
    " appear in the data."
   ],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%% md\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}