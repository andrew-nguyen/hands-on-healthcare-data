#!/usr/bin/env bash

# UMLS_DIR needs to be mapped to the Docker container as a shared volume
# Docker example scripts in the GitLab repo assumes the default below
UMLS_DIR="${HOME}/data/sources/umls/2021AB/META"

MRCONSO_FILE="${UMLS_DIR}/MRCONSO.RRF"

FILE_PREFIX="MRCONSO_20k_"

rm -f ${UMLS_DIR}/${FILE_PREFIX}*

cp MRCONSO.header ${UMLS_DIR}/

# split files since the arangoimport command seems to timeout if batches are
# too large
echo "Splitting files in ${UMLS_DIR}"

split -d -l 20000 ${MRCONSO_FILE} ${UMLS_DIR}/${FILE_PREFIX}

start_time=$(date +%s)

LOG_FILE="umls-full-import-$(date -Iseconds -d "@${start_time}").log"

eval "echo Starting at: $(date -Iseconds -d "@${start_time}")" | tee ${LOG_FILE}

for a in $(ls ${UMLS_DIR}/${FILE_PREFIX}* | xargs -n 1 basename); do
  echo $a
  docker exec -it umls-arangodb sh -c \
  "arangoimport \
  --file /var/sources/umls/2021AB/META/$a \
  --headers-file /var/sources/umls/2021AB/META/MRCONSO.header \
  --type csv \
  --separator \| \
  --quote $'\x1B' \
  --collection umls_concepts \
  --create-collection \
  --ignore-missing \
  --log.level error \
  --server.password '' \
  --overwrite false"
done | tee -a ${LOG_FILE}

end_time=$(date +%s)

elapsed=$(( end_time - start_time ))

eval "echo Ending at: $(date -Iseconds -d "@${end_time}")" | tee -a ${LOG_FILE}

eval "echo Elapsed time: $(date -ud "@$elapsed" +'$((%s/3600/24)) days %H hr %M min %S sec')" | tee -a ${LOG_FILE}


rm -f ${UMLS_DIR}/${FILE_PREFIX}*
