#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
#
from database import graph


def get_arango(dbname, truncate=False):
    return graph.ArangoDB(username="",
                          password="",
                          url="http://127.0.0.1:8529",
                          dbname=dbname,
                          truncate=truncate)
