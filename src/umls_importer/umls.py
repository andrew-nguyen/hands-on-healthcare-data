#  Copyright (c) 2022-2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT


from enum import Enum
import json
import pandas as pd
import requests
from random import randint

from database import graphdb, graph


class UMLSAPI:
    def __init__(self, auth):
        self.uts_base = "https://uts-ws.nlm.nih.gov/rest"
        self.rxnorm_base = "https://rxnav.nlm.nih.gov/REST"
        self.auth = auth

    def get_content_view(self, cui):
        uri = self.uts_base + f"/content-views/current/CUI/{cui}"
        query = {'ticket': self.auth.getst()}
        r = requests.get(uri, params=query)
        r.encoding = "utf-8"

        if r.status_code == 200:
            body = json.loads(r.text)
        else:
            body = None

        return r.status_code, body

    def get_content_view_members(self, cui, pageSize=5):
        uri = self.uts_base + f"/content-views/current/CUI/{cui}/members"
        query = {
            'ticket': self.auth.getst(),
            'pageSize': pageSize
        }
        r = requests.get(uri, params=query)
        r.encoding = "utf-8"

        if r.status_code == 200:
            body = json.loads(r.text)
        else:
            body = None

        return r.status_code, body

    def get_NDC_properties(self, ndc):
        uri = self.rxnorm_base + "/ndcproperties.json"
        query = {
            'id': ndc
        }
        r = requests.get(uri, params=query)
        r.encoding = 'utf-8'

        if r.status_code == 200:
            body = json.loads(r.text)
        else:
            body = {}

        return r.status_code, body

    def get_NDC_status(self, ndc):
        uri = self.rxnorm_base + "/ndcstatus.json"
        query = {
            'ndc': ndc
        }
        r = requests.get(uri, params=query)
        r.encoding = 'utf-8'

        if r.status_code == 200:
            body = json.loads(r.text)
        else:
            body = {}

        return r.status_code, body


def rxcui_from_ndc_properties(ndc_resp):
    if ndc_resp[1]:
        return ndc_resp[1]['ndcPropertyList']['ndcProperty'][0]['rxcui']
    else:
        return ''


def rxcui_from_ndc_status(ndc_resp):
    if ndc_resp[1]:
        return ndc_resp[1]['ndcStatus']['rxcui']
    else:
        return ''


def conceptName_from_ndc_status(ndc_resp):
    if ndc_resp[1]:
        return ndc_resp[1]['ndcStatus']['conceptName']
    else:
        return ''


def read_rxcui_csv(path):
    data = pd.read_csv(path, dtype=str)
    return data


def clean_nan(x):
    return None if pd.isna(x) else x


# Generally, corresponds to an entry in MRCONSO.RRF though this is a WIP and
# additional fields/columns will be added as time progresses
# https://www.ncbi.nlm.nih.gov/books/NBK9685/table/ch03.T.concept_names_and_sources_file_mr/
class UMLSConcept(graph.GraphConcept):
    def __str__(self):
        if self.rxcui:
            cui_str = f"RXCUI: {self.rxcui}"
        else:
            cui_str = f"CUI: {self.cui}"

        return f"{self.str} ({cui_str})"

    def __repr__(self):
        if self.rxcui:
            cui_str = f"RXCUI: {self.rxcui}"
        else:
            cui_str = f"CUI: {self.cui}"

        return f"{self.str} ({cui_str})"

    def __init__(self, cui=None, rxcui=None, str=None, ndc=None):
        self.rxcui = clean_nan(rxcui)
        self.cui = clean_nan(cui)
        self.ndc = clean_nan(ndc)
        self.lat = None
        self.ts = None
        self.lui = None
        self.stt = None
        self.sui = None
        self.ispref = None
        self.aui = None
        self.saui = None
        self.scui = None
        self.sdui = None
        self.sab = None
        self.tty = None
        self.code = None
        self.str = str
        self.srl = None
        self.suppress = None
        self.cvf = None

    def as_dict(self):
        keys = ['cui', 'str', 'rxcui', 'ndc']
        return {k: vars(self)[k] for k in keys}

    def to_arango(self, collection: Enum) -> graph.ArangoQuery:
        bind = {"doc": self.as_dict()}
        aql = f"INSERT @doc INTO {collection.value} LET newDoc = NEW " \
              f"RETURN newDoc"
        return {"aql": aql, "bind_vars": bind}

    def to_sparql(
            self,
            coll: Enum,
            id=randint(1000000, 9999999)
    ) -> graph.SPARQLQuery:

        prop_str = [f"hohcd:{id} {graphdb.ns_lookup[k]} \"{v}\" ."
                    for k, v in filter(lambda x: x[1] is not None,
                                       self.as_dict().items())]

        return {
            'query': """
INSERT DATA {{
    hohcd:{id} rdf:type hohcd:{type} .
    {props}
}}
""".format(id=id,
           type=coll.value,
           props="\n    ".join(prop_str)).strip()
        }
