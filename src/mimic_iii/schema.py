#  Copyright (c) 2022. Andrew Nguyen
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
#
class Prescription:
    def __init__(self):
        self.row_id = None
        self.subject_id = None
        self.hadm_id = None
        self.icustay_id = None
        self.startdate = None
        self.enddate = None
        self.drug_type = None
        self.drug = None
        self.drug_name_poe = None
        self.drug_name_generic = None
        self.formulary_drug_cd = None
        self.gsn = None
        self.ndc = None
        self.prod_strength = None
        self.dose_val_rx = None
        self.dose_unit_rx = None
        self.form_val_disp = None
        self.form_unit_disp = None
        self.route = None
