#  Copyright (c) 2022-2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT


from enum import Enum
import pandas as pd

import database.graph as graph


class MIMIC_Importable(graph.GraphConcept):
    def __init__(self, row):
        self.dict = row.to_dict(dict())

    def to_arango(self, collection: Enum) -> graph.ArangoQuery:
        self.dict['_key'] = self.dict['row_id']
        bind = {"doc": self.dict}
        aql = f"INSERT @doc INTO {collection.value} LET newDoc = NEW " \
              f"RETURN newDoc"
        return {"aql": aql, "bind_vars": bind}


def import_mimic_row(row: pd.Series):
    d = row.to_dict(dict())
    d['_key'] = d['row_id']
