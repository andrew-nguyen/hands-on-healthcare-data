#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
from arango import ArangoClient
from arango.http import DefaultHTTPClient
import psycopg2

from enum import Enum
from typing import Any, TypedDict, Dict, List, Union


def validate(value, msg):
    if value == None:
        raise ValueError(msg)
    else:
        return value


class GraphConcept:
    def to_arango(self, collection_name):
        raise NotImplementedError()

    def to_neo4j(self):
        raise NotImplementedError()


class ArangoQuery(TypedDict):
    aql: str
    bind_vars: dict[str, Any]


class SPARQLQuery(TypedDict):
    query: str


class Default_Collections(Enum):
    CONCEPTS = 'concepts'
    PRESCRIPTIONS = 'prescriptions'


Default_Indexes: dict[Default_Collections, list[str]] = {
    Default_Collections.CONCEPTS: ['rxcui',
                                   'ndc'],
    Default_Collections.PRESCRIPTIONS: ['ndc',
                                        'row_id',
                                        'subject_id',
                                        'hadm_id',
                                        'icustay_id']
}


class Default_Edges(Enum):
    INSTANCE_OF = "instance_of"
    MAPS_TO = "maps_to"


Default_Edge_Definitions = {
    Default_Edges.INSTANCE_OF: {
        'from': [Default_Collections.PRESCRIPTIONS],
        'to': [Default_Collections.CONCEPTS]
    },
    Default_Edges.MAPS_TO: {
        'from': [Default_Collections.CONCEPTS],
        'to': [Default_Collections.CONCEPTS]
    }
}


class Claims_Collections(Enum):
    ALLERGIES = 'allergies'
    CAREPLANS = 'careplans'
    CLAIMS = 'claims'
    CLAIMS_TRANSACTIONS = 'claims_transactions'
    CONDITIONS = 'conditions'
    DEVICES = 'devices'
    ENCOUNTERS = 'encounters'
    IMAGING_STUDIES = 'imaging_studies'
    IMMUNIZATIONS = 'immunizations'
    MEDICATIONS = 'medications'
    OBSERVATIONS = 'observations'
    ORGANIZATIONS = 'organizations'
    PATIENTS = 'patients'
    PAYER_TRANSITIONS = 'payer_transitions'
    PAYERS = 'payers'
    PROCEDURES = 'procedures'
    PROVIDERS = 'providers'
    SUPPLIES = 'supplies'


Claims_Indexes: dict[Claims_Collections, list[str]] = {

}


class Claims_Edges(Enum):
    LINKED_TO = 'linked_to'


Claims_Edge_Definitions = {
    Claims_Edges.LINKED_TO: {
        'from': [Claims_Collections.ALLERGIES,
                 Claims_Collections.CAREPLANS,
                 Claims_Collections.CLAIMS,
                 Claims_Collections.CLAIMS_TRANSACTIONS,
                 Claims_Collections.CONDITIONS,
                 Claims_Collections.DEVICES,
                 Claims_Collections.ENCOUNTERS,
                 Claims_Collections.IMAGING_STUDIES,
                 Claims_Collections.IMMUNIZATIONS,
                 Claims_Collections.MEDICATIONS,
                 Claims_Collections.OBSERVATIONS,
                 Claims_Collections.ORGANIZATIONS,
                 Claims_Collections.PATIENTS,
                 Claims_Collections.PAYER_TRANSITIONS,
                 Claims_Collections.PAYERS,
                 Claims_Collections.PROCEDURES,
                 Claims_Collections.PROVIDERS,
                 Claims_Collections.SUPPLIES],
        'to': [Claims_Collections.PATIENTS,
               Claims_Collections.ENCOUNTERS,
               Claims_Collections.PAYERS,
               Claims_Collections.PROVIDERS,
               Claims_Collections.CLAIMS,
               Claims_Collections.ORGANIZATIONS,
               Claims_Collections.PAYER_TRANSITIONS]
    },
}

All_Collections = Union[Default_Collections, Claims_Collections]
All_Edges = Union[Default_Edges, Claims_Edges]


class PostgresDB:
    def __init__(self,
                 username='mimic',
                 password='password',
                 dbname='mimic'):
        # Initialize the client for ArangoDB.
        self.conn = psycopg2.connect(f"host=localhost "
                                     f"dbname={dbname} "
                                     f"user={username} "
                                     f"password={password}")

    def drop_database(self):
        raise NotImplementedError()

    def insert(self, coll: All_Collections, d: dict):
        return self.execute({
            'aql': f'INSERT @doc INTO {coll.value} LET newDoc = NEW RETURN newDoc',
            'bind_vars': {'doc': d}})

    def execute(self, q, data=None):
        cur = self.conn.cursor()
        cur.execute(q, data)
        return cur.fetchall()

    def link_ndc(self):
        pass


class ArangoDB:
    def __init__(self,
                 username=None,
                 password=None,
                 url=None,
                 dbname=None,
                 truncate=False,
                 collections=Default_Collections,
                 indexes=Default_Indexes,
                 edge_definitions=Default_Edge_Definitions):
        self.username = validate(username, "Username not specified")
        self.password = validate(password, "Password not specified")
        self.url = validate(url, "URL not specified")
        self.dbname = validate(dbname, "Database name not specified")

        http_client = DefaultHTTPClient()
        http_client.REQUEST_TIMEOUT = 600  # 10 mins

        # Initialize the client for ArangoDB.
        self.client = ArangoClient(hosts=self.url,
                                   http_client=http_client)

        # Connect to "_system" database as root user.
        self.sys_db = self.client.db("_system",
                                     username=self.username,
                                     password=self.password)

        if truncate and self.sys_db.has_database(self.dbname):
            self.sys_db.delete_database(self.dbname)

        if not self.sys_db.has_database(self.dbname):
            self.sys_db.create_database(self.dbname)

        self.db = self.client.db(self.dbname,
                                 username=self.username,
                                 password=self.password)

        graph_name = 'chapter4'
        if self.db.has_graph(graph_name):
            self.graph = self.db.graph(graph_name)
        else:
            self.graph = self.db.create_graph(graph_name)

        self.collections = {}
        self.indexes = {}
        self.edges = {}

        self.init(collections, indexes, edge_definitions, truncate)

    def init(self, collections, indexes, edge_definitions, truncate=False):
        # Create vertex collections
        for c in collections:
            if not self.graph.has_vertex_collection(c.value):
                # print(f"Creating collection: {c}")
                self.collections[c] = self.graph.create_vertex_collection(
                    c.value
                )
            else:
                # print(f"Collection {c} already exists")
                self.collections[c] = self.graph.vertex_collection(c.value)

                if truncate:
                    print(f"truncating... {c}")
                    self.collections[c].truncate()
                else:
                    print(f"Not truncating... {c}")

            # Setup indexes for each collection
            if c in indexes:
                self.indexes[c] = self.collections[c].add_hash_index(
                    fields=indexes[c]
                )

        # Setup edge collections
        for e, d in edge_definitions.items():
            if not self.graph.has_edge_definition(e.value):
                self.edges[e] = self.graph.create_edge_definition(
                    edge_collection=e.value,
                    from_vertex_collections=[x.value for x in d['from']],
                    to_vertex_collections=[x.value for x in d['to']]
                )

    def collection(self, c: All_Collections):
        return self.collections[c]

    def edge(self, e: All_Edges):
        return self.edges[e]

    def drop_database(self):
        self.sys_db.delete_database(self.dbname)

    def insert(self, coll: All_Collections, d: dict):
        return self.execute({
            'aql': f'INSERT @doc INTO {coll.value} LET newDoc = NEW RETURN newDoc',
            'bind_vars': {'doc': d}})

    def execute(self, a: ArangoQuery, ttl=60):
        return self.db.aql.execute(a['aql'],
                                   bind_vars=a['bind_vars'],
                                   ttl=ttl)

    def link_ndc(self):
        pass
