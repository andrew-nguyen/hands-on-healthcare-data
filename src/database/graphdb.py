#  Copyright (c) 2022. Andrew Nguyen
#
#  Use of this source code is governed by an MIT-style
#  license that can be found in the LICENSE file or at
#  https://opensource.org/licenses/MIT

#
from SPARQLWrapper import SPARQLWrapper, JSON
from random import randint

import json
import requests

from database import graph

ns = {
    'bioportal': "http://purl.bioontology.org/ontology/",
    'rxnorm': "http://purl.bioontology.org/ontology/RXNORM/",
    'skos': "http://www.w3.org/2004/02/skos/core#",
    'hohcd': "http://hands-on-healthcare-data/"
}

ns_lookup = {
    'rxcui': "rxnorm:RXCUI",
    'ndc': "rxnorm:NDC",
    'cui': "bioportal:cui",
    'str': "skos:prefLabel"
}


def prefixes():
    return "\n".join([f"PREFIX {k}: <{v}>" for k, v in ns.items()])


DEFAULT_REPO_ID = "hands-on-healthcare-data"

DEFAULT_REPO_CONFIG = {
    # id was changed from default
    "id": "hands-on-healthcare-data",
    "params": {
        "queryTimeout": {
            "label": "Query timeout (seconds)",
            "name": "queryTimeout",
            "value": 0
        },
        "cacheSelectNodes": {
            "label": "Cache select nodes",
            "name": "cacheSelectNodes",
            "value": "true"
        },
        "rdfsSubClassReasoning": {
            "label": "RDFS subClass reasoning",
            "name": "rdfsSubClassReasoning",
            "value": "true"
        },
        "undefinedTargetValidatesAllSubjects": {
            "label": "Validate subjects when target is undefined",
            "name": "undefinedTargetValidatesAllSubjects",
            "value": "false"
        },
        "validationEnabled": {
            "label": "Enable the SHACL validation",
            "name": "validationEnabled",
            "value": "true"
        },
        "parallelValidation": {
            "label": "Run parallel validation",
            "name": "parallelValidation",
            "value": "true"
        },
        "title": {
            "label": "Repository description",
            "name": "title",
            "value": "GraphDB Free repository"
        },
        "checkForInconsistencies": {
            "label": "Enable consistency checks",
            "name": "checkForInconsistencies",
            "value": "false"
        },
        "performanceLogging": {
            "label": "Log the execution time per shape",
            "name": "performanceLogging",
            "value": "false"
        },
        "disableSameAs": {
            "label": "Disable owl:sameAs",
            "name": "disableSameAs",
            "value": "true"
        },
        "entityIndexSize": {
            "label": "Entity index size",
            "name": "entityIndexSize",
            "value": "10000000"
        },
        "dashDataShapes": {
            "label": "DASH data shapes extensions",
            "name": "dashDataShapes",
            "value": "true"
        },
        "queryLimitResults": {
            "label": "Limit query results",
            "name": "queryLimitResults",
            "value": 0
        },
        "throwQueryEvaluationExceptionOnTimeout": {
            "label": "Throw exception on query timeout",
            "name": "throwQueryEvaluationExceptionOnTimeout",
            "value": "false"
        },
        "id": {
            "label": "Repository ID",
            "name": "id",
            "value": "repo-test"
        },
        "validationResultsLimitPerConstraint": {
            "label": "Validation results limit per constraint",
            "name": "validationResultsLimitPerConstraint",
            "value": 10
        },
        "storageFolder": {
            "label": "Storage folder",
            "name": "storageFolder",
            "value": "storage"
        },
        "enablePredicateList": {
            "label": "Enable predicate list index",
            "name": "enablePredicateList",
            "value": "true"
        },
        "logValidationPlans": {
            "label": "Log the executed validation plans",
            "name": "logValidationPlans",
            "value": "false"
        },
        "imports": {
            "label": "Imported RDF files(';' delimited)",
            "name": "imports",
            "value": ""
        },
        "inMemoryLiteralProperties": {
            "label": "Cache literal language tags",
            "name": "inMemoryLiteralProperties",
            "value": "true"
        },
        "isShacl": {
            "label": "Enable SHACL validation",
            "name": "isShacl",
            "value": "false"
        },
        "ruleset": {
            "label": "Ruleset",
            "name": "ruleset",
            "value": "rdfsplus-optimized"
        },
        "ignoreNoShapesLoadedException": {
            "label": "Ignore no shapes loaded exception",
            "name": "ignoreNoShapesLoadedException",
            "value": "false"
        },
        "readOnly": {
            "label": "Read-only",
            "name": "readOnly",
            "value": "false"
        },
        "enableLiteralIndex": {
            "label": "Enable literal index",
            "name": "enableLiteralIndex",
            "value": "true"
        },
        "defaultNS": {
            "label": "Default namespaces for imports(';' delimited)",
            "name": "defaultNS",
            "value": ""
        },
        "enableContextIndex": {
            "label": "Enable context index",
            "name": "enableContextIndex",
            "value": "false"
        },
        "logValidationViolations": {
            "label": "Log validation violations",
            "name": "logValidationViolations",
            "value": "false"
        },
        "baseURL": {
            "label": "Base URL",
            "name": "baseURL",
            # value was changed from default
            "value": "http://hands-on-healthcare-data/graphdb#"
        },
        "globalLogValidationExecution": {
            "label": "Log every execution step of the SHACL validation",
            "name": "globalLogValidationExecution",
            "value": "false"
        },
        "entityIdSize": {
            "label": "Entity ID size",
            "name": "entityIdSize",
            "value": "32"
        },
        "eclipseRdf4jShaclExtensions": {
            "label": "RDF4J SHACL extensions",
            "name": "eclipseRdf4jShaclExtensions",
            "value": "true"
        },
        "validationResultsLimitTotal": {
            "label": "Validation results limit total",
            "name": "validationResultsLimitTotal",
            "value": 1000
        },
        "repositoryType": {
            "label": "Repository type",
            "name": "repositoryType",
            "value": "file-repository"
        }
    },
    # title was changed from default
    "title": "Hands-On Healthcare Data",
    "type": "free"
}


class GraphDB:
    def __init__(self, id = "hands-on-healthcare-data"):
        endpoint = f"http://localhost:7200/repositories/{id}"

        self.read_endpoint = endpoint
        self.write_endpoint = f"{endpoint}/statements"
        self.read_sparql = SPARQLWrapper(self.read_endpoint)
        self.write_sparql = SPARQLWrapper(self.write_endpoint)

        if not GraphDB.repository_exists(id):
            GraphDB.create_repository(id)

    def repository_exists(id: str) -> bool:
        r = requests.get(
            url=f"http://localhost:7200/rest/repositories/{id}",
            headers={"Accept": "application/json"}
        )

        return r.status_code == 200

    def delete_repository(id: str):
        r = requests.delete(
            url=f"http://localhost:7200/rest/repositories/{id}",
            headers={"Accept": "application/json"}
        )

        # endpoint always returns 200
        return True

    def create_repository(
            id: str = "hands-on-healthcare-data",
            title: str = "Hands-On Healthcare Data",
            baseurl: str = "http://hands-on-healthcare-data/graphdb#"
    ) -> bool:
        # given the number of parameters, uses a JSON file that was extracted
        # via chrome dev tools when creating a repo using the workbench web UI
        # using nearly all default values
        d = DEFAULT_REPO_CONFIG
        d['id'] = id
        d['title'] = title
        d['params']['baseURL']['value]'] = baseurl
        r = requests.post(
            url="http://localhost:7200/rest/repositories",
            headers={"Content-Type": "application/json"},
            data=json.dumps(d)
        )

        if r.status_code == 201:
            return True
        else:
            return False

    def insert(self, coll: graph.Default_Collections, d: dict):
        id = randint(1000000, 9999999)
        prop_str = [f"hohcd:{id} {ns_lookup[k]} \"{v}\" ."
                    for k, v in filter(lambda x: x[1] is not None,
                                       d.items())]

        query = {
            'query': """
        CREATE GRAPH hohcd:graph-{id};
        INSERT DATA {{
            GRAPH hohcd:graph-{id} {{
                hohcd:{id} rdf:type hohcd:{type} .
                {props}
            }}
        }}
        """.format(id=id,
                   type=coll.value,
                   props="\n    ".join(prop_str)).strip()
        }

        return self.execute(query, query_type='WRITE')

    def execute(self, query: graph.SPARQLQuery, query_type='READ'):
        q_str = prefixes() + "\n" + query['query']
        # print(f"Query:\n{q_str}")

        if query_type == 'WRITE':
            sparql = self.write_sparql
            sparql.method = 'POST'
        else:
            sparql = self.read_sparql
            sparql.method = 'GET'

        sparql.setQuery(q_str)
        sparql.setReturnFormat(JSON)
        return sparql.query().convert()
