#
# Copyright (c) 2022. Andrew Nguyen
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT
#

db._query("
FOR x in allergies
    FOR y in patients
        FILTER x.PATIENT == y.ID

        INSERT {
                _from: x._id,
                _to: y._id

            } in linked_to
        RETURN NEW
")
